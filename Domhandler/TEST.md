# text-encoding单元测试用例

单元测试用例覆盖情况

|          接口名          |是否通过	|备注|
|:---------------------:|:---:|:---:|
|         isTag         |pass   |        |
|        isCDATA        |pass   |        |
|        isText         |pass   |        |
|       isComment       |pass   |        |
|      isDirective      |pass   |        |
|      isDocument       |pass  |     |
|      hasChildren      |   pass  |          |
|       cloneNode       | pass |  |
|        Element        | pass  |       |
|      firstChild       |  pass |          |
|       lastChild       |  pass |          |
|      childNodes       | pass  |          |
| ProcessingInstruction | pass  |          |
|        parent         |  pass |          |
|         prev          |  pass |          |
|         next          | pass  |          |
|      startIndex       | pass  |          |
|      endIndex         | pass  |          |



