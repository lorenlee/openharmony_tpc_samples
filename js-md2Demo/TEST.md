# js-md2单元测试用例

该测试用例基于OpenHarmony系统下，采用[原库测试用例](https://github.com/emn178/js-md2/tree/master/tests)进行单元测试

单元测试用例覆盖情况

|   接口名    |是否通过	|备注|
|:--------:|:---:|:---:|
| md2() |pass||