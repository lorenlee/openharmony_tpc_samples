## behavior 单元测试用例

该测试用例基于 OpenHarmony 系统下，采用[原库测试用例](https://github.com/Calamari/BehaviorTree.js/tree/master/src) 进行单元测试

### 单元测试用例覆盖情况

| 接口名               | 是否通过 | 备注 |
| -------------------- | -------- | ---- |
| BehaviorTree         | pass     |
| SUCCESS              | pass     |
| FAILURE              | pass     |
| RUNNING              | pass     |
| getRegistry          | pass     |
| registryLookUp       | pass     |
| BehaviorTreeImporter | pass     |
| BranchNode           | pass     |
| Node                 | pass     |
| Parallel             | pass     |
| ParallelComplete     | pass     |
| ParallelSelector     | pass     |
| Selector             | pass     |
| Sequence             | pass     |
| Random               | pass     |
| Decorator            | pass     |
| Task                 | pass     |
| Introspector         | pass     |
| decorators           | pass     |
