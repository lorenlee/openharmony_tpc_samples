## v2.0.0
- 适配DevEco Studio 版本： 4.1 Canary(4.1.3.317), OpenHarmony SDK:API11 (4.1.0.36)

## 0.1.1
1.适配DevEco Studio 3.1 Beta1版本。

## 0.1.0
1.支持数字、大数、三角函数、字符串、和矩阵等数学运算功能。
