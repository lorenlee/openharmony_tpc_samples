## js-tokens单元测试用例

该测试用例基于OpenHarmony系统下，采用[原库测试用例](https://github.com/lydell/js-tokens/archive/refs/tags/v5.0.0.zip) 进行单元测试

### 单元测试用例覆盖情况

| 接口名                   | 是否通过 |备注|
|-----------------------|---|---|
| jsTokens.default      |pass|
| jsTokens.matchToToken |pass|