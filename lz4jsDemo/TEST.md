## lz4js单元测试用例

该测试用例基于OpenHarmony系统下，采用[原库测试用例](https://github.com/Benzinga/lz4js/tree/master/test/cases) 进行单元测试

### 单元测试用例覆盖情况

| 接口名                | 是否通过 |备注|
|--------------------|---|---|
| decompress()       |pass|
| decompressBlock()  |pass|
| compress()         |pass|
| makeBuffer()          |pass|
| hash()          |pass|