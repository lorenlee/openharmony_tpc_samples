/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* eslint-env node, mocha */
import xxh32 from 'lz4js/xxh32';
import { describe, beforeAll, beforeEach, afterEach, afterAll, it, expect } from '@ohos/hypium'

export default function xxh32Test() {
  // fromCharCode, but understands right > 0xffff values
   let a2s=(array:number)=> {
    return String.fromCharCode(0,array);
  }

  describe('xxh32Test',  () => {
    beforeAll( () => {

    })
    beforeEach( () => {

    })
    afterEach( () => {
      // Presets a clear action, which is performed after each unit test case ends.
      // The number of execution times is the same as the number of test cases defined by **it**.
      // This API supports only one parameter: clear action function.
    })
    afterAll( () => {
      // Presets a clear action, which is performed after all test cases of the test suite end.
      // This API supports only one parameter: clear action function.
    })

    it('passesTheXxHashTests', 0, () => {
      let tests = [
        [0x02cc5d05, []],
        [0x550d7456, [0x61]],
        [0x4999fc53, [0x61, 0x62]],
        [0x32d153ff, [0x61, 0x62, 0x63]],
        [0xa3643705, [0x61, 0x62, 0x63, 0x64]],
        [0x9738f19b, [0x61, 0x62, 0x63, 0x64, 0x65]],
        [0x8b7cd587, [0x61, 0x62, 0x63, 0x64, 0x65, 0x66]],
        [0x9dd093b3, [0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67]],
        [0x0bb3c6bb, [0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68]],
        [0xd03c13fd, [0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68,
          0x69]],
        [0x8b988cfe, [0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68,
          0x69, 0x6a]],
        [0x9d2d8b62, [0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68,
          0x69, 0x6a, 0x6b, 0x6c, 0x6d, 0x6e, 0x6f, 0x70]],
        [0x42ae804d, [0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68,
          0x69, 0x6a, 0x6b, 0x6c, 0x6d, 0x6e, 0x6f, 0x70,
          0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78,
          0x79, 0x7a, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35,
          0x36, 0x37, 0x38, 0x39]],
        [0x62b4ed00, [0x4c, 0x6f, 0x72, 0x65, 0x6d, 0x20, 0x69, 0x70,
          0x73, 0x75, 0x6d, 0x20, 0x64, 0x6f, 0x6c, 0x6f,
          0x72, 0x20, 0x73, 0x69, 0x74, 0x20, 0x61, 0x6d,
          0x65, 0x74, 0x2c, 0x20, 0x63, 0x6f, 0x6e, 0x73,
          0x65, 0x63, 0x74, 0x65, 0x74, 0x75, 0x72, 0x20,
          0x61, 0x64, 0x69, 0x70, 0x69, 0x73, 0x63, 0x69,
          0x6e, 0x67, 0x20, 0x65, 0x6c, 0x69, 0x74, 0x2c,
          0x20, 0x73, 0x65, 0x64, 0x20, 0x64, 0x6f, 0x20,
          0x65, 0x69, 0x75, 0x73, 0x6d, 0x6f, 0x64, 0x20,
          0x74, 0x65, 0x6d, 0x70, 0x6f, 0x72, 0x20, 0x69,
          0x6e, 0x63, 0x69, 0x64, 0x69, 0x64, 0x75, 0x6e,
          0x74, 0x20, 0x75, 0x74, 0x20, 0x6c, 0x61, 0x62,
          0x6f, 0x72, 0x65, 0x20, 0x65, 0x74, 0x20, 0x64,
          0x6f, 0x6c, 0x6f, 0x72, 0x65, 0x20, 0x6d, 0x61,
          0x67, 0x6e, 0x61, 0x20, 0x61, 0x6c, 0x69, 0x71,
          0x75, 0x61, 0x2e, 0x20, 0x55, 0x74, 0x20, 0x65,
          0x6e, 0x69, 0x6d, 0x20, 0x61, 0x64, 0x20, 0x6d,
          0x69, 0x6e, 0x69, 0x6d, 0x20, 0x76, 0x65, 0x6e,
          0x69, 0x61, 0x6d, 0x2c, 0x20, 0x71, 0x75, 0x69,
          0x73, 0x20, 0x6e, 0x6f, 0x73, 0x74, 0x72, 0x75,
          0x64, 0x20, 0x65, 0x78, 0x65, 0x72, 0x63, 0x69,
          0x74, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x20, 0x75,
          0x6c, 0x6c, 0x61, 0x6d, 0x63, 0x6f, 0x20, 0x6c,
          0x61, 0x62, 0x6f, 0x72, 0x69, 0x73, 0x20, 0x6e,
          0x69, 0x73, 0x69, 0x20, 0x75, 0x74, 0x20, 0x61,
          0x6c, 0x69, 0x71, 0x75, 0x69, 0x70, 0x20, 0x65,
          0x78, 0x20, 0x65, 0x61, 0x20, 0x63, 0x6f, 0x6d,
          0x6d, 0x6f, 0x64, 0x6f, 0x20, 0x63, 0x6f, 0x6e,
          0x73, 0x65, 0x71, 0x75, 0x61, 0x74, 0x2e, 0x20,
          0x44, 0x75, 0x69, 0x73, 0x20, 0x61, 0x75, 0x74,
          0x65, 0x20, 0x69, 0x72, 0x75, 0x72, 0x65, 0x20,
          0x64, 0x6f, 0x6c, 0x6f, 0x72, 0x20, 0x69, 0x6e,
          0x20, 0x72, 0x65, 0x70, 0x72, 0x65, 0x68, 0x65,
          0x6e, 0x64, 0x65, 0x72, 0x69, 0x74, 0x20, 0x69,
          0x6e, 0x20, 0x76, 0x6f, 0x6c, 0x75, 0x70, 0x74,
          0x61, 0x74, 0x65, 0x20, 0x76, 0x65, 0x6c, 0x69,
          0x74, 0x20, 0x65, 0x73, 0x73, 0x65, 0x20, 0x63,
          0x69, 0x6c, 0x6c, 0x75, 0x6d, 0x20, 0x64, 0x6f,
          0x6c, 0x6f, 0x72, 0x65, 0x20, 0x65, 0x75, 0x20,
          0x66, 0x75, 0x67, 0x69, 0x61, 0x74, 0x20, 0x6e,
          0x75, 0x6c, 0x6c, 0x61, 0x20, 0x70, 0x61, 0x72,
          0x69, 0x61, 0x74, 0x75, 0x72, 0x2e, 0x20, 0x45,
          0x78, 0x63, 0x65, 0x70, 0x74, 0x65, 0x75, 0x72,
          0x20, 0x73, 0x69, 0x6e, 0x74, 0x20, 0x6f, 0x63,
          0x63, 0x61, 0x65, 0x63, 0x61, 0x74, 0x20, 0x63,
          0x75, 0x70, 0x69, 0x64, 0x61, 0x74, 0x61, 0x74,
          0x20, 0x6e, 0x6f, 0x6e, 0x20, 0x70, 0x72, 0x6f,
          0x69, 0x64, 0x65, 0x6e, 0x74, 0x2c, 0x20, 0x73,
          0x75, 0x6e, 0x74, 0x20, 0x69, 0x6e, 0x20, 0x63,
          0x75, 0x6c, 0x70, 0x61, 0x20, 0x71, 0x75, 0x69,
          0x20, 0x6f, 0x66, 0x66, 0x69, 0x63, 0x69, 0x61,
          0x20, 0x64, 0x65, 0x73, 0x65, 0x72, 0x75, 0x6e,
          0x74, 0x20, 0x6d, 0x6f, 0x6c, 0x6c, 0x69, 0x74,
          0x20, 0x61, 0x6e, 0x69, 0x6d, 0x20, 0x69, 0x64,
          0x20, 0x65, 0x73, 0x74, 0x20, 0x6c, 0x61, 0x62,
          0x6f, 0x72, 0x75, 0x6d, 0x2e]]
      ];

      for (let i = 0; i < tests.length; ++i) {
        let expected = tests[i][0];
        let vector= tests[i][1];
        let a=vector as Array<number>
          expect(xxh32.hash(0, vector, 0, a.length)).assertEqual(expected);
      }
    });
  })
}