## v2.0.0
- 适配DevEco Studio 版本： 4.1 Canary(4.1.3.317), OpenHarmony SDK:API11 (4.1.0.36)
- ArkTs新语法适配

## v1.0.1

- 适配DevEco Studio 版本：3.1 Beta1（3.1.0.200），OpenHarmony SDK:API9（3.2.10.6）

## v1.0.0

- 已实现功能
  - 发布消息

  - 消费消息

  - 交换机配置：默认交换机、直连交换机、扇形交换机、主题交换机

  - 多队列匹配

  - 一对一连接

  - 超时检查