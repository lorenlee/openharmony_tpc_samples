# events单元测试用例

该测试用例基于OpenHarmony系统下，进行单元测试

单元测试用例覆盖情况

|               接口名               | 是否通过	 | 备注  |
|:-------------------------------:|:-----:|:---:|
|  on(topic: string,cb:Function)  | pass  |     |
|  emit(topic:string,msg:string)  | pass  |     |
|        off(topic:string)        | pass  |     |
| once(topic: string,cb:Function) | pass  |     |

